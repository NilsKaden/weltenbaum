import React, { useEffect } from 'react';

const Crew = (props) => {
  const { setActive } = props;

  useEffect(() => setActive('crew'));

  return (
    <div>
      <p>Crew</p>
    </div>
  );
};

export default Crew;
