import React from 'react';
import { Link } from 'react-router-dom';
import styles from '../assets/styles/Header.module.scss';

const Header = (props) => {
  const { pages } = props;

  return (
    <header className={styles.header}>
      {pages.map((e, i )=> <Link to={`/${e}`} className={styles.item} key={`${i} ${e}`}>{e}</Link>)}
    </header>
  );
};

export default Header;
