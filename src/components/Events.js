import React, { useEffect } from 'react';

const Events = (props) => {
  const { setActive } = props;

  useEffect(() => setActive('events'));

  return (
    <div>
      <p>Events</p>
    </div>
  );
};

export default Events;
