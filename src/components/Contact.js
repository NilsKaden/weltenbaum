import React, { useEffect } from 'react';

const Contact = (props) => {
  const { setActive } = props;
  useEffect(() => setActive('contact'));

  return (
    <div>
      <p>Contact</p>
    </div>
  );
};

export default Contact;
