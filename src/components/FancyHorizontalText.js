import React from 'react';
import styles from '../assets/styles/FancyHorizontalText.module.scss';

const FancyHorizontalText = (props) => {
  const { text } = props;
  const letters  = Array.from(text);

  return (
    <div className={styles.container} alt='text'>
      {letters.map((e, i) => <p className={styles.letter} key={`${i} ${e}`}>{e}</p>)}
    </div>
  );
};

export default FancyHorizontalText;