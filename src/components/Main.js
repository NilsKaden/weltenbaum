import React, { useState } from 'react';
import { Switch, Route } from 'react-router-dom';
import Header from './Header';
import Crew from './Crew';
import Events from './Events';
import Contact from './Contact';
import styles from '../assets/styles/Main.module.scss';
import FancyHorizontalText from './FancyHorizontalText';

const Main = () => {
  const [activePage, setActivePage] = useState('crew');

  return (
    <div className={styles.app}>
      <main className={styles.main}>
        <div className={styles.sidebar}>
          <FancyHorizontalText text={activePage}/>
        </div>
        <div className={styles.content}>
          <div className={styles.header}>
            <Header pages={['events', 'contact', 'crew']}/>
          </div>
          <Switch>
            <Route exact path='/' render={() => <Crew setActive={setActivePage}/>}/>
            <Route path='/events' render={() => <Events setActive={setActivePage}/>}/>
            <Route path='/contact' render={() => <Contact setActive={setActivePage}/>}/>
            <Route path='/crew' render={() => <Crew setActive={setActivePage}/>}/>
          </Switch>
        </div>
      </main>
    </div>
  );
};

export default Main; 
